```sh
docker-compose build
```

```sh
docker-compose up
```

```sh
docker ps
```

находим <container_id> контейнера php

```sh
docker exec -it <container_id> bash
```

```sh
cp .env.example .env
```

```sh
composer install
```

```sh
php artisan key:generate
```

```sh
php artisan migrate
```
```sh
npm install
```
```sh
npm run dev
```

```sh
php artisan test
```

Открыть в браузере: http://localhost:8000

![Scrin app](scrinshot.png)
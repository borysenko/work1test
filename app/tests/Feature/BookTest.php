<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Book;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_create_book(): void
    {
        $response = $this->post('/api/book', [
            'title' => 'Test',
            'publisher' => 'Test',
            'author' => 'Test',
            'genre' => 'Test',
            'amount' => 1,
            'price' => 1,
        ]);

        $response->assertStatus(201);
    }

    public function test_books(): void
    {
        $response = $this->get('/api/books');

        $response->assertStatus(200);
    }

    public function test_book(): void
    {
        $id = Book::latest('id')->first()->id;
        
        $response = $this->get('/api/book/'. $id);

        $response->assertStatus(200);
    }

    public function test_patch_book(): void
    {
        $id = Book::latest('id')->first()->id;
        
        $response = $this->patch('/api/book/'. $id, [
            'title' => 'Test2',
            'publisher' => 'Test2',
            'author' => 'Test2',
            'genre' => 'Test2',
            'amount' => 12,
            'price' => 12,
        ]);

        $response->assertStatus(200);
    }

    public function test_delete_book(): void
    {
        $id = Book::latest('id')->first()->id;
        
        $response = $this->delete('/api/book/'. $id);

        $response->assertStatus(200);
    }
}

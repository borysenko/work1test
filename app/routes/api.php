<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;

Route::middleware('api')->group(function () {
    Route::get('/books', [BookController::class, 'index']);
    Route::post('/book', [BookController::class, 'store']);
    Route::get('/book/{id}', [BookController::class, 'show']);
    Route::patch('/book/{id}', [BookController::class, 'update']);
    Route::delete('/book/{id}', [BookController::class, 'destroy']);
});
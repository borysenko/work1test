<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BookService;
use App\Http\Requests\StoreBookRequest;

class BookController extends Controller
{
    public function __construct(
        protected BookService $bookService
      ) {
      }
  
      public function index()
      {
          $books = $this->bookService->all();
          return $books;
      }
  
      public function store(StoreBookRequest $request)
      {
          $data = $request->validated();
  
          $book = $this->bookService->create($data);
  
          return $book;
      }
  
      public function show($id)
      {
          $book = $this->bookService->find($id);
          return $book;
      }
  
      public function update(StoreBookRequest $request, $id)
      {
          $data = $request->validated();
  
          $book = $this->bookService->update($data, $id);
  
          return $book;
      }
  
      public function destroy($id)
      {
          $book = $this->bookService->delete($id);
  
          return $book;
      }
}

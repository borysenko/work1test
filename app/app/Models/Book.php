<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    const CREATED_AT = 'publication';
    const UPDATED_AT = null;

    //protected $casts = ['publication'];

    protected $fillable = [
        'title',
        'publisher',
        'author',
        'genre',
        'amount',
        'price',
    ];

}
